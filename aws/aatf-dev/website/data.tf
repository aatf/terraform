data "aws_secretsmanager_secret" "website" {
  name = "website"
}

data "aws_secretsmanager_secret_version" "website" {
  secret_id = data.aws_secretsmanager_secret.website.id
}
