module "website" {
  source = "../../../../terraform-modules/aws/website"

  bootstrap_bucket = "aatf-dev"
  dev_mode         = true
  acm_domain_name  = "dev.aatf.us"
  acm_sans = [
    "www.dev.aatf.us",
    "score.dev.aatf.us",
    "scorer.dev.aatf.us",
    "scores.dev.aatf.us",
  ]

}
