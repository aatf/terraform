remote_state {
  backend = "s3"
  config = {
    profile        = "aatf-dev"
    bucket         = "terraform-state-aatf-dev"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "terraform-state-lock"
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "skip"

  contents = <<EOF
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }
  }
}

provider "aws" {
  profile = "aatf-dev"

  region = var.region
}
EOF
}

generate "variables" {
  path      = "variables.tf"
  if_exists = "skip"

  contents = <<EOF
variable "region" {
  type        = string
  description = "region"

  default = ""
}
EOF
}

generate "outputs" {
  path      = "outputs.tf"
  if_exists = "skip"

  contents = ""
}
