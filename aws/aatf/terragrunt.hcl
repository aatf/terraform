remote_state {
  backend = "s3"
  config = {
    profile        = "aatf"
    bucket         = "terraform-state-aatf"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "terraform-state-lock"
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "skip"

  contents = <<EOF
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.50"
    }

    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 4.0"
    }
  }
}

provider "aws" {
  alias = east

  profile = "aatf"
  region  = "us-east-1"
}

provider "aws" {
  profile = "aatf"

  region = var.region
}

provider "cloudflare" {
  api_token = ""
}
EOF
}

generate "variables" {
  path      = "variables.tf"
  if_exists = "skip"

  contents = <<EOF
variable "region" {
  type        = string
  description = "region"

  default = ""
}
EOF
}

generate "outputs" {
  path      = "outputs.tf"
  if_exists = "skip"

  contents = ""
}
