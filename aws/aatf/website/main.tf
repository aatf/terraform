module "website" {
  source = "../../../../terraform-modules/aws/website"

  image            = "public.ecr.aws/e3k2e0k8/website:2023.7.10"
  bootstrap_bucket = "aatf-bootstrap"
  cdn_domain_name  = "cdn.aatf.us"
  cdn_bucket_name  = "aatf-us"
  acm_domain_name  = "aatf.us"
  acm_sans = [
    "www.aatf.us",
    "score.aatf.us",
    "scorer.aatf.us",
    "scores.aatf.us",
  ]

  providers = {
    aws.east = aws.east
  }
}
